#include <SoftwareSerial.h>

SoftwareSerial BT(10, 11); // RX, TX
SoftwareSerial XB(2,3); //RX, TX

void setup(){
  Serial.begin(9600); //Computer Port for Testing
  if(Serial.available()){
    Serial.print("Testing Start");
  }
  mySerial.begin(9600); //Bluetooth
  XB.begin(9600); //ZigBee
}

void loop(){
  if (BT.available()) {
    XB.write(BT.read());
  }
  // Pair Phone -> BlueTooth -> ZigBee -> Other Phone
  if (XB.available()) {
    BT.write(XB.read());
  }
  // Other Phone -> ZigBee -> BlueTooth -> Pair Phone
}