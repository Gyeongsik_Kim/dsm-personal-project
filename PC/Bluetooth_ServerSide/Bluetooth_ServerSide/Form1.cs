﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using InTheHand.Net.Sockets;
using InTheHand.Net.Bluetooth;
using InTheHand.Windows.Forms;
using InTheHand.Net.Bluetooth.AttributeIds;
using System.IO;
using System.Security.Cryptography;

namespace Bluetooth_ServerSide
{
    public partial class Form1 : Form
    {
        // Threads
        Thread AcceptAndListeningThread;
        // helper variable
        Boolean isConnected = false;

        //Password
        private static String Password = "GyungDal_Encryption_Key";
        //bluetooth stuff
        BluetoothClient btClient;  //represent the bluetooth client connection
        BluetoothListener btListener; //represent this server bluetooth device

        public Form1()
        {

            InitializeComponent();
            this.Text = "Bluetooth Chat";

            //when the bluetooth is supported by this computer

            if (BluetoothRadio.IsSupported)
            {

                UpdateLogText("Bluetooth Supported!");
                UpdateLogText("—————————–");

                //getting device information
                UpdateLogText("Primary Bluetooth Radio Name : " + BluetoothRadio.PrimaryRadio.Name);
                UpdateLogText("Primary Bluetooth Radio Address : " + BluetoothRadio.PrimaryRadio.LocalAddress);
                UpdateLogText("Primary Bluetooth Radio Manufacturer : " + BluetoothRadio.PrimaryRadio.Manufacturer);
                UpdateLogText("Primary Bluetooth Radio Mode : " + BluetoothRadio.PrimaryRadio.Mode);
                UpdateLogText("Primary Bluetooth Radio Version : " + BluetoothRadio.PrimaryRadio.LmpVersion);
                UpdateLogText("Primary Bluetooth Radio Software Manufacturer : " + BluetoothRadio.PrimaryRadio.SoftwareManufacturer);
                UpdateLogText("—————————–");

                //creating and starting the thread
                AcceptAndListeningThread = new Thread(AcceptAndListen);

                AcceptAndListeningThread.Start();

            }
            else
            {
                UpdateLogText("Bluetooth not Supported!");
            }
        }
        StreamReader srReceiver;
        private delegate void UpdateLogCallback(string strMessage);

        private void ReceiveMessages()
        {
            // Receive the response from the server
            srReceiver = new StreamReader(btClient.GetStream());

            // If the first character of the response is 1, connection was successful
            string ConResponse = srReceiver.ReadLine();

            // If the first character is a 1, connection was successful
            if (ConResponse[0] == '1')
            {
                // Update the form to tell it we are now connected
                this.Invoke(new UpdateLogCallback(this.UpdateLogText), new object[] { "Connected Successfully!" });
            }
            else // If the first character is not a 1 (probably a 0), the connection was unsuccessful
            {
                string Reason = "Not Connected: ";

                // Extract the reason out of the response message. The reason starts at the 3rd character
                Reason += ConResponse.Substring(2, ConResponse.Length - 2);

                // Exit the method
                return;
            }
            // While we are successfully connected, read incoming lines from the server
            while (isConnected)
            {
                // Show the messages in the log TextBox
                this.Invoke(new UpdateLogCallback(this.UpdateLogText), new object[] { srReceiver.ReadLine() });
            }
        }

        //the function of the thread
        public void AcceptAndListen()
        {
            while (true)
            {
                if (isConnected)
                {
                    //TODO: if there is a device connected
                    //listening
                    try
                    {
                        UpdateLogTextFromThread("Listening….");
                        NetworkStream stream = btClient.GetStream();

                        Byte[] bytes = new Byte[512];

                        String retrievedMsg = "";

                        stream.Read(bytes, 0, 512);

                        stream.Flush();

                        for (int i = 0; i < bytes.Length; i++)
                        {
                            retrievedMsg += Convert.ToChar(bytes[i]);

                        }
                        retrievedMsg = Decrypt(retrievedMsg);
                        UpdateLogTextFromThread(btClient.RemoteMachineName + " : " + retrievedMsg);
                        UpdateLogTextFromThread("");

                        if (!retrievedMsg.Contains("servercheck"))
                        {

                            sendMessage("Message Received!");
                        }
                        ReceiveMessages();
                    }
                    catch (Exception ex)
                    {
                        UpdateLogTextFromThread("There is an error while listening connection");
                        UpdateLogTextFromThread(ex.Message);
                        isConnected = btClient.Connected;
                    }
                }
                else
                {
                    //TODO: if there is no connection
                    // accepting
                    try
                    {
                        btListener = new BluetoothListener(BluetoothService.RFCommProtocol);

                        UpdateLogTextFromThread("Listener created with TCP Protocol service " + BluetoothService.RFCommProtocol);
                        UpdateLogTextFromThread("Starting Listener….");
                        btListener.Start();
                        UpdateLogTextFromThread("Listener Started!");
                        UpdateLogTextFromThread("Accepting incoming connection….");
                        btClient = btListener.AcceptBluetoothClient();
                        isConnected = btClient.Connected;
                        UpdateLogTextFromThread("A Bluetooth Device Connected!");
                    }
                    catch (Exception e)
                    {
                        UpdateLogTextFromThread("There is an error while accepting connection");
                        UpdateLogTextFromThread(e.Message);
                        UpdateLogTextFromThread("Retrying….");
                    }
                }
            }
        }
        //this section is to create a method that allow thread accessing form’s component
        //we can’t update the text of the textbox directly from thread, so, we use this delegate function

        delegate void UpdateLogTextFromThreadDelegate(String msg);
        public void UpdateLogTextFromThread(String msg)
        {
            if (!this.IsDisposed && logsText.InvokeRequired)
            {
                try {
                    logsText.Invoke(new UpdateLogTextFromThreadDelegate(UpdateLogText), new Object[] { msg });
                }catch(InvalidOperationException e){
                    MessageBox.Show(e.Message);
                }catch(Exception e){
                    MessageBox.Show(e.Message);
                }
            }
        }
        //just ordinary function to update the log text.
        //after updating, we move the cursor to the end of text and scroll it to the cursor.
        public void UpdateLogText(String msg)
        {
            logsText.Text += msg + Environment.NewLine;
            logsText.SelectionStart = logsText.Text.Length;
            logsText.ScrollToCaret();
        }
        //function to send message to the client
        public Boolean sendMessage(String msg)
        {
            try
            {
                if (!msg.Equals(""))
                {
                    UTF8Encoding encoder = new UTF8Encoding();
                    NetworkStream stream = btClient.GetStream();
                    String send = Encrypt(msg);
                    stream.Write(encoder.GetBytes(msg + "\n"), 0, encoder.GetBytes(send).Length);
                    stream.Flush();

                }
            }
            catch (Exception ex)
            {
                UpdateLogTextFromThread("There is an error while sending message");
                UpdateLogTextFromThread(ex.Message);
                try
                {
                    isConnected = btClient.Connected;
                    btClient.GetStream().Close();
                    btClient.Dispose();
                    btListener.Server.Dispose();
                    btListener.Stop();
                }
                catch (Exception)
                {
                }

                return false;
            }

            return true;
        }
        //when closing or exiting application, we have to close connection and aborting the thread.
        //Otherwise, the process of the thread still running in the background.
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                AcceptAndListeningThread.Abort();
                btClient.GetStream().Close();
                btClient.Dispose();
                btListener.Stop();
                FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            }
            catch (Exception)
            {
            }
        }

        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }
        private void sendBtn_Click(object sender, EventArgs e)
        {
            sendMessage(messageText.Text);
        }
        public String Encrypt(String InputText)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(InputText);

            // 딕셔너리 공격을 대비해서 키를 더 풀기 어렵게 만들기 위해서   
            // Salt를 사용한다.  
            byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);

            // Create a encryptor from the existing SecretKey bytes.  
            // encryptor 객체를 SecretKey로부터 만든다.  
            // Secret Key에는 32바이트  
            // Initialization Vector로 16바이트를 사용  
            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

            MemoryStream memoryStream = new MemoryStream();

            // CryptoStream객체를 암호화된 데이터를 쓰기 위한 용도로 선언  
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

            cryptoStream.Write(PlainText, 0, PlainText.Length);

            cryptoStream.FlushFinalBlock();

            byte[] CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string EncryptedData = Convert.ToBase64String(CipherBytes);

            return EncryptedData;
        }

        //AES_256 복호화  
        public String Decrypt(String InputText)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] EncryptedData = Convert.FromBase64String(InputText);
            byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);

            //Make Decryptor class
            ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

            MemoryStream memoryStream = new MemoryStream(EncryptedData);

            //cryptoStream class for data read
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

            //Make byte array for encryptedata save
            byte[] PlainText = new byte[EncryptedData.Length];

            int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

            memoryStream.Close();
            cryptoStream.Close();

            string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

            return DecryptedData;
        }
    }
}


