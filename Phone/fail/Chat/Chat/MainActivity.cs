﻿ using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using Android.Bluetooth;
namespace Chat
{
	[Activity (Label = "Setup PassWord", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{	
		protected string Key;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);
			EditText Pass = FindViewById<EditText> (Resource.Id.Pass);
			Button Next = FindViewById<Button> (Resource.Id.Next);
			Next.Click += Button_Click;
		}

		private void Button_Click(object sender, EventArgs e)
		{
			EditText Pass = FindViewById<EditText> (Resource.Id.Pass);
			if (Pass.Text == "") {
				Toast.MakeText (ApplicationContext, "키를 입력해주세요.", ToastLength.Short).Show ();
			} else {
				Toast.MakeText (ApplicationContext, "암호화 키 : " + Pass.Text, ToastLength.Short).Show ();
				Key = Pass.Text;
				SetContentView(Resource.Layout.Chat);
			}
		}
		public string EncryptText(string input){
			byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
			byte[] passwordBytes = Encoding.UTF8.GetBytes(Key);
			passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
			byte[] encryptedBytes = null;
			// Set your salt here, change it to meet your flavor:
			// The salt bytes must be at least 8 bytes.
			byte[] saltBytes = new byte[] { 0,0,0,0,0,0,0,0};

			using (MemoryStream ms = new MemoryStream())
			{
				using (RijndaelManaged AES = new RijndaelManaged())
				{
					AES.KeySize = 256;
					AES.BlockSize = 128;

					var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
					AES.Key = key.GetBytes(AES.KeySize / 8);
					AES.IV = key.GetBytes(AES.BlockSize / 8);

					AES.Mode = CipherMode.CBC;

					using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
					{
						cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
						cs.Close();
					}
					encryptedBytes = ms.ToArray();
				}
			}
			return Convert.ToBase64String(encryptedBytes);
		}

		//AES_256 복호화  
		public string DecryptText(string input){
			// Get the bytes of the string
			byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
			byte[] passwordBytes = Encoding.UTF8.GetBytes(Key);
			passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
			byte[] decryptedBytes = null;
			// Set your salt here, change it to meet your flavor:
			// The salt bytes must be at least 8 bytes.
			byte[] saltBytes = new byte[] { 0,0,0,0,0,0,0,0};
			using (MemoryStream ms = new MemoryStream())
			{
				using (RijndaelManaged AES = new RijndaelManaged())
				{
					AES.KeySize = 256;
					AES.BlockSize = 128;

					var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
					AES.Key = key.GetBytes(AES.KeySize / 8);
					AES.IV = key.GetBytes(AES.BlockSize / 8);

					AES.Mode = CipherMode.CBC;

					using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
					{
						cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
						cs.Close();
					}
					decryptedBytes = ms.ToArray();
				}
			}
			return Encoding.UTF8.GetString(decryptedBytes);
		}

	}
}


